<?php
session_start();
require_once './../../vendor/autoload.php';
use App\User\User;
use \PDO;
$user = new User();

if($user->is_loggedin()!="")
{
	$user->redirect('home.php');
}

if(isset($_POST['btn-signup']))
{
	$uname = strip_tags($_POST['username']);
	$upass = strip_tags($_POST['password']);
	$umail = strip_tags($_POST['email']);	
	
	if($uname=="")	{
		$error[] = "provide username !";	
	}
        else if($upass=="")	{
		$error[] = "provide password !";
	}
	else if(strlen($upass) < 6){
		$error[] = "Password must be atleast 6 characters";	
	}
	else if($umail=="")	{
		$error[] = "provide email id !";	
	}
	else if(!filter_var($umail, FILTER_VALIDATE_EMAIL))	{
	    $error[] = 'Please enter a valid email address !';
	}
	
	else
	{
		try
		{
			$stmt = $user->runQuery("SELECT username, email FROM users WHERE username=:uname OR email=:umail");
			$stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
			$row=$stmt->fetch(PDO::FETCH_ASSOC);
				
			if($row['username']==$uname) {
				$error[] = "sorry username already taken !";
			}
			else if($row['email']==$umail) {
				$error[] = "sorry email id already taken !";
			}
			else
			{
				if($user->register($uname,$upass,$umail)){	
					$user->redirect('sign-up.php?joined');
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sign up</title>
<link href="../../assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="../../assets/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="../../assets/css/app.css" type="text/css"  />
    <link rel="stylesheet" href="../../assets/css/style.css" type="text/css"  />
</head>
<body>

<div class="signin-form">

<div class="container">
    	
        <form method="post" class="form-signin">
            <h2 class="form-signin-heading">Sign up.</h2><hr />
            <?php
			if(isset($error))
			{
			 	foreach($error as $error)
			 	{
					 ?>
                     <div class="alert alert-danger">
                        <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php  echo $error; ?>
                     </div>
                     <?php
				}
			}
			else if(isset($_GET['joined']))
			{
				 ?>
                 <div class="alert alert-info">
                     <i class="glyphicon glyphicon-log-in"></i> &nbsp; Successfully registered <a href='../../index.php'>login</a> here
                 </div>
                 <?php
			}
			?>
            <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Enter Username" value="<?php  if(isset($error)){echo $uname;}?>" />
            </div>
            <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Enter Password" />
            </div>
            <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder="Enter E-Mail ID" value="<?php  if(isset($error)){echo $umail;}?>" />
            </div>
            
            <div class="clearfix"></div><hr />
            <div class="form-group">
            	<button type="submit" class="btn btn-primary" name="btn-signup">
                	<i class="glyphicon glyphicon-open-file"></i>&nbsp;SIGN UP
                </button>
            </div>
            <br />
            <label>have an account ! <a href="../../index.php">Sign In</a></label>
        </form>
       </div>
</div>

</div>

</body>
</html>